export type studentBase = {
    id: number,
    username: number,
    name: string,
    phone: number,
    faculty_name: string,
    email: string,
    avatar: string,
    student_code: number,
    admission_year: number,
    fcm_token: string,
    advisor_id: number,
    monitor_id: number,
    secretary_id: number,
    birth_day: string,
    class_type: string,
    class_name: string,
    address: string,
    degree: string,
    sex: string,
    role: {
        name: string,
        access: string
    }

    //         "id": 3,
    //         "username": 14520333,
    //         "name": "Khánh Hùng",
    //         "phone": 18006601,
    //         "faculty_name": "MMTT",
    //         "email": "14520333@gm.uit.edu.vn",
    //         "avatar": "https://s3-ap-southeast-1.amazonaws.com/imageavatar-bucket/student-male-3.jpg",
    //         "student_code": 14520333,
    //         "admission_year": 2014,
    //         "fcm_token": "random_string_notification_is_not_yet_implemented",
    //         "advisor_id": 3,
    //         "monitor_id": 2,
    //         "secretary_id": 3,
    //         "birth_day": "15-01-1996",
    //         "class_type": "CQUI",
    //         "class_name": "MMTT2014",
    //         "address": "KTX khu B, Khu phố 6, Phường Linh Trung, Quận Thủ Đức, TPHCM",
    //         "degree": "Đại Học",
    //         "sex": "Nam",
    //         "role": {
    //             "name": "secretary",
    //             "access": "[\"module_registration\", \"compensation\", \"year_planning\", \"student_absence\", \"fee\", \"contact\", \"advanced_secretary_1\"]"
    //         }

};