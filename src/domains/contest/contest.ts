export type contest = {
    // name: string,
    // es: [esItem]
    mamh: string,
    ngaythi: string,
    thu: number,
    ca: number,
    malop: string,
    id: number,
    phongthi: string,
    tenmh: string,
};

// "mamh": "NT118",
// "ngaythi": "07-01-2019",
// "thu": "2",
// "ca": "3",
// "malop": "NT118.J11",
// "id": "37676",
// "phongthi": "B7.02",
// "tenmh": "Phát triển ứng dụng trên thiết bị di động"