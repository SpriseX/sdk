export type contestBase = {
    day_of_the_week: number,
    on_day: string,
    period: number,
    room_name: string,
    supervisory: string,
    ordinal_number: number,
    module_registration: {
        class_room: {
            class_alias: string,
            subject: string,
            year: number,
            teacher: {
                name: string,
            }
        }
    }
};



// {
//     "data": [
//         {
//             "id": 1,
//             "module_registration_id": 1,
//             "ordinal_number": 41,
//             "day_of_the_week": 3,
//             "on_day": "2018-09-17",
//             "period": 3,
//             "room_name": "C200",
//             "supervisory": "Trần Kim Trọng|Phan Thục Phương",
//             "module_registration": {
//                 "updated_at": "2018-09-16 16:01:55",
//                 "class_room": {
//                     "id": 1,
//                     "class_alias": "T001.J02",
//                     "subject": "Nhập môn mạch số",
//                     "year": 2018,
//                     "teacher": {
//                         "name": "Lê Đức Hùng"
//                     }
//                 }
//             }
//         },
//         {
//             "id": 2,
//             "module_registration_id": 2,
//             "ordinal_number": 29,
//             "day_of_the_week": 4,
//             "on_day": "2018-09-18",
//             "period": 3,
//             "room_name": "C139",
//             "supervisory": "Ngô Đức Tuyền|Nguyễn Quỳnh Hương",
//             "module_registration": {
//                 "updated_at": "2018-09-16 16:01:55",
//                 "class_room": {
//                     "id": 3,
//                     "class_alias": "T003.J04",
//                     "subject": "Lập trình hệ thống",
//                     "year": 2018,
//                     "teacher": {
//                         "name": "Lê Đức Hùng"
//                     }
//                 }
//             }
//         },
//         {
//             "id": 3,
//             "module_registration_id": 3,
//             "ordinal_number": 34,
//             "day_of_the_week": 5,
//             "on_day": "2018-09-19",
//             "period": 5,
//             "room_name": "C158",
//             "supervisory": "Nguyễn Kiến Tường|Ngô Khánh Quỳnh",
//             "module_registration": {
//                 "updated_at": "2018-09-16 16:01:55",
//                 "class_room": {
//                     "id": 4,
//                     "class_alias": "T005.J06",
//                     "subject": "Cấu trúc dữ liệu và giải thuật",
//                     "year": 2018,
//                     "teacher": {
//                         "name": "Lê Đức Hùng"
//                     }
//                 }
//             }
//         },
//         {
//             "id": 4,
//             "module_registration_id": 4,
//             "ordinal_number": 19,
//             "day_of_the_week": 6,
//             "on_day": "2018-09-20",
//             "period": 4,
//             "room_name": "C298",
//             "supervisory": "Phan Đức An|Nguyễn Cát Tiên",
//             "module_registration": {
//                 "updated_at": "2018-09-16 16:01:55",
//                 "class_room": {
//                     "id": 6,
//                     "class_alias": "N001.J02",
//                     "subject": "Nhập môn mạch số",
//                     "year": 2018,
//                     "teacher": {
//                         "name": "Nguyễn Mạnh Phát"
//                     }
//                 }
//             }
//         },
//         {
//             "id": 5,
//             "module_registration_id": 5,
//             "ordinal_number": 4,
//             "day_of_the_week": 7,
//             "on_day": "2018-09-21",
//             "period": 5,
//             "room_name": "C108",
//             "supervisory": "Ngô Đức Tuyền|Phan Thục Phương",
//             "module_registration": {
//                 "updated_at": "2018-09-16 16:01:55",
//                 "class_room": {
//                     "id": 7,
//                     "class_alias": "N005.J06",
//                     "subject": "Lập trình hệ thống",
//                     "year": 2018,
//                     "teacher": {
//                         "name": "Lê Đức Hùng"
//                     }
//                 }
//             }
//         },
//         {
//             "id": 6,
//             "module_registration_id": 6,
//             "ordinal_number": 13,
//             "day_of_the_week": 2,
//             "on_day": "2018-09-22",
//             "period": 4,
//             "room_name": "C186",
//             "supervisory": "Ngô Đức Tuyền|Trần Thiên Hương",
//             "module_registration": {
//                 "updated_at": "2018-09-16 16:01:55",
//                 "class_room": {
//                     "id": 8,
//                     "class_alias": "N006.J07",
//                     "subject": "Cấu trúc dữ liệu và giải thuật",
//                     "year": 2018,
//                     "teacher": {
//                         "name": "Lê Đức Hùng"
//                     }
//                 }
//             }
//         }
//     ],
//     "error_code": "0000",
//     "error_message": "Get Contest success"
// }