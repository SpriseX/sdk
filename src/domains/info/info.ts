export type info = {
    name: string,
    sid: string,
    mail: string,
    status: number,
    course: number,
    major: string,
    dob: string,
    role: string,
    class: string,
    address: string,
    avatar: string,
};

// "name": "Phan Thanh Huy",
// "sid": "14520382",
// "mail": "14520382@gm.uit.edu.vn",
// "status": "1",
// "course": "9",
// "major": "Truyền thông và Mạng máy tính (D480102)",
// "dob": "19/11/1996",
// "role": "SV",
// "class": "MMTT2014",
// "address": "39/4 Trần Nguyên Hãn"