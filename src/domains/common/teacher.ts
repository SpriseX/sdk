export type teacher = {
    codes: string,
    name: string,
    phone: number,
    faculty_name: string,
    teacher_email: string,
};
