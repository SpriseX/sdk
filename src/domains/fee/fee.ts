export type fee = {
    phaidong: number,
    notruoc: number,
    dadong: number,
    hocky: number,
    namhoc: number,
    somon: number,
    dkhp: string,
    thoigiandong: string,
    nganhang: string,
};


// {
//     "phaidong": "4800000",
//     "notruoc": "0",
//     "dadong": "4800000",
//     "hocky": "1",
//     "namhoc": "2018",
//      "somon": "1",
//      "dkhp": "NT505(15.0)",
//      "thoigiandong": "2018-10-15",
//      "nganhang": "ACB",

// },

// "phaidong": "4800000",
// "somon": "1",
// "dkhp": "NT505(15.0)",
// "thoigiandong": "2018-10-15",
// "nganhang": "ACB",
// "notruoc": "0",
// "dadong": "4800000",
// "hocky": "1",
// "namhoc": "2018"

