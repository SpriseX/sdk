export type feeBase = {
    title: string,
    semester: number,
    year: number,
    price: number,
    price_to_pay: number,
    last_semester_debt: number,
    have_paid: number,
    description: string,
    module_registrations: string,
    credits: number,
    number_of_subjects: number,
    detail: string,
    paid_time: string,
    bank: string,

    // "data": [
    //     {
    //         "id": 1,
    //         "student_id": 1,
    //         "title": "Học kỳ 1|Năm học 2014-2015",
    //         "semester": 1,
    //         "year": 2015,
    //         "price": 1250000,
    //         "price_to_pay": 1250000,
    //         "last_semester_debt": 0,
    //         "have_paid": 1250000,
    //         "description": "SV nghèo vượt khó",
    //         "module_registrations": "T001, T002, T003, T004",
    //         "credits": 15
    //     },
};
