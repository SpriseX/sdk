export type moduleRegistrationBase = {
    id: number,
    class_alias: string,
    class: string,
    subject: string,
    room_name: string;
    period: number;
    start_time: string;
    end_time: string;
    on_week: string;
    year: number;
    semester: number;
    day_of_the_week: number;
    credits: number;
    faculty: string;
    teacher: any,
    language: string;
    teaching_type: string;

    // gravatar: (size: number) => string

    // {
    //     "data": [
    //         {
    //             "updated_at": "2018-09-11 17:19:58",
    //             "class_room": {
    //                 "id": 4,
    //                 "class_alias": "T004.J05",
    //                 "class": "T004",
    //                 "subject": "Nhập môn mạch số",
    //                 "room_name": "C196",
    //                 "period": "[1, 2, 3]",
    //                 "start_time": "07:30:00",
    //                 "end_time": "10:30:00",
    //                 "on_week": "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]",
    //                 "year": 2018,
    //                 "semester": 1,
    //                 "day_of_the_week": 5,
    //                 "credits": 5,
    //                 "faculty": "MMTT",
    //                 "teacher": {
    //                     "name": "Nguyễn Mạnh Phát",
    //                     "phone": 1886196916,
    //                     "faculty_name": "MMTT",
    //                     "email": "phatnm@uit.edu.vn"
    //                 }
    //             }
    //         },
    //         {
    //             "updated_at": "2018-09-11 17:19:58",
    //             "class_room": {
    //                 "id": 8,
    //                 "class_alias": "N002.J03",
    //                 "class": "N002",
    //                 "subject": " Cơ sở dữ liệu",
    //                 "room_name": "C218",
    //                 "period": "[7, 8]",
    //                 "start_time": "13:30:00",
    //                 "end_time": "17:30:00",
    //                 "on_week": "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]",
    //                 "year": 2018,
    //                 "semester": 1,
    //                 "day_of_the_week": 3,
    //                 "credits": 4,
    //                 "faculty": "KTMT",
    //                 "teacher": {
    //                     "name": "Nguyễn Mạnh Phát",
    //                     "phone": 1886196916,
    //                     "faculty_name": "MMTT",
    //                     "email": "phatnm@uit.edu.vn"
    //                 }
    //             }
    //         }
    //     ],
    //     "error_code": "0000",
    //     "error_message": "Get module registration success"
    // }
};
