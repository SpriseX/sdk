export type transcript = {
    diem: diem[],
    diemtb: diemtb
};

export type diem = {
    id: number,
    masv: string,
    mamh: string,
    malop: string,
    sotc: number,
    namhoc: number,
    hocky: number,
    diem1: number,
    diem2: number,
    diem3: number,
    diem4: number,
    diem: number,
    thoigiancapnhat: string,
    trangthai: string,
    heso1: number,
    heso2: number,
    heso3: number,
    heso4: number,
    tinhtrang: string,
    mamh_tt: string,
    ghichu: string,
    tenmh: string,
    loaimh: string
};

export type diemtb = {
    dtb: number,
    dtbtl: number,
    tctl: number
};

    // "diem": [
    //     {
    //         "id": "470434",
    //         "masv": "14520382",
    //         "mamh": "PH001",
    //         "malop": "PH001.I31",
    //         "sotc": "3",
    //         "namhoc": "2017",
    //         "hocky": "3",
    //         "diem1": "10",
    //         "diem2": null,
    //         "diem3": "9",
    //         "diem4": "9",
    //         "diem": "9.1",
    //         "thoigiancapnhat": "2018-09-02 21:12:52",
    //         "trangthai": "1",
    //         "heso1": "0.1",
    //         "heso2": "0",
    //         "heso3": "0.2",
    //         "heso4": "0.7",
    //         "tinhtrang": "2",
    //         "mamh_tt": null,
    //         "ghichu": null,
    //         "tenmh": "Nhập môn điện tử",
    //         "loaimh": "ĐC"
    //     }
    // ],
    // "diemtb": {
    //     "dtb": 9.1,
    //     "dtbtl": 9.1,
    //     "tctl": 3
    // }

