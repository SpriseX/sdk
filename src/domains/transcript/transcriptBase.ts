export type transcriptBase = {
    semester: number,
    title: string,
    class: string,
    class_name: string,
    class_code: string,
    credits: number,
    process_mark: number,
    middle_mark: number,
    end_mark: number,
    lab_mark: number,
    final_mark: number,
    note: string,

    year: number,

    // {
    // 	"data": [{
    // 		"semester_number": 1,
    // 		"title": "Học kì 1|Năm học 2014-2015",
    // 		"class": "EN001",
    // 		"class_name": "Anh văn 1",
    // 		"credits": 3,
    // 		"process_mark": 0.8,
    // 		"middle_mark": 7.6,
    // 		"end_mark": 0.8,
    // 		"lab_mark": 8.5,
    // 		"final_mark": 6.5,
    // 		"note": ""
    // 	}],
    // 	"error_code": "0000",
    // 	"error_message": "Get Transcript success"
    // }
};
