export class Responser {
    static getCustomSuccessMessage(data: any, baseData: any, message: string) {
        return {
            data: data,
            base_data: baseData,
            error_code: "0000",
            error_message: message,
        };
    }

    static getFailMessageByServer(error: Error) {
        if (error.message === "Request failed with status code 403") {
            return {
                error_code: "0002",
                error_message: "Token is expired",
            };
        }

        return {
            error_code: "0002",
            error_message: "General error",
            error_message_extra: error.message,
        };
    }

    static getCustomFailMessageByServer(message: string) {
        return {
            error_code: "0002",
            error_message: message
        };
    }

    static getCustomFailMessageByClient(message: string) {
        return {
            error_code: "0001",
            error_message: message
        };
    }

    static getTokenSuccessMessage(token: string, logoutToken: string, data: object, baseData: object) {
        return {
            data: {
                student: data,
                token: token,
                logout_token: logoutToken,
            },
            base_data: baseData,
            error_code: "0000",
            error_message: "Get token success",
        };
    }
}