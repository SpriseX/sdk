import async, { forEach } from "async";
import crypto from "crypto";
import nodemailer from "nodemailer";
import passport from "passport";
import { default as User, UserModel, AuthToken } from "../models/User";
import { Request, Response, NextFunction } from "express";
import { IVerifyOptions } from "passport-local";
import { WriteError } from "mongodb";
import axios from "axios";
import moment from "moment";


import "../config/passport";
import { moduleRegistration } from "../domains/moduleRegistration/moduleRegistration";
import { moduleRegistrationBase } from "../domains/moduleRegistration/moduleRegistrationBase";

import { transcript } from "../domains/transcript/transcript";
import { transcriptBase } from "../domains/transcript/transcriptBase";

import { student } from "../domains/student/student";
import { studentBase } from "../domains/student/studentBase";

import { fee } from "../domains/fee/fee";
import { feeBase } from "../domains/fee/feeBase";

import { info } from "../domains/info/info";
import { infoBase } from "../domains/info/infoBase";

import { contest } from "../domains/contest/contest";
import { contestBase } from "../domains/contest/contestBase";

import { teacher } from "../domains/common/teacher";

import { Responser } from "./Responser";

const request = require("express-validator");


/**
 * POST api/login
 * Login.
 */
export let apiPostLogin = (req: Request, res: Response) => {
  const { username, password } = req.body;

  if (Number.isInteger(username) || Number.isInteger(password)) {
    return res.send(Responser.getCustomFailMessageByClient("Username or password should be string"));
  }

  console.log(username, "-", password, " is logging in");

  // Crawl generic
  axios.post("http://crawler:8081/api/crawl_generic", {
    username,
    password,
  });

  // Crawl deadline
  axios.post("http://python_crawler:5000/api/crawl_deadline", {
    username,
    password,
  });

  // Log user in
  axios.post("https://apiservice.uit.edu.vn/v1/user/token", {
    username,
    password
  }).then(response => {
    axios.post("https://apiservice.uit.edu.vn/v1/user/login", {
      username,
      password
    }, {
        headers: { "X-CSRF-Token": response.data.token }
      }).then(response => {
        const data = response.data.user as student;
        const baseData = {
          id: Number(data.uid),
          username: data.name,
          name: data.fname.slice(0, data.fname.lastIndexOf(" -")),
          phone: -1,
          faculty_name: "unknown",
          email: data.mail,
          avatar: "unknown",
          student_code: -1,
          admission_year: -1,
          fcm_token: "unknown",
          advisor_id: -1,
          monitor_id: -1,
          secretary_id: -1,
          birth_day: "unknown",
          class_type: "unknown",
          class_name: "unknown",
          address: "unknown",
          degree: "unknown",
          sex: "unknown",
          role: {
            name: "unknown",
            access: "[\"module_registration\", \"transcript\", \"get_deadline\"]"
          }
        } as studentBase;
        res.send(Responser.getTokenSuccessMessage(response.headers["set-cookie"][0], response.data.token, baseData, response.data));



      }).catch(error => {
        res.send(Responser.getCustomFailMessageByServer(error.response.data[0]));
      });
  }).catch(error => {
    res.send(Responser.getFailMessageByServer(error));
  });
};

/**
 * POST api/logout
 * Logout.
 */
export let apiPostLogout = (req: Request, res: Response) => {
  const { username, password } = req.body;
  axios.post("https://apiservice.uit.edu.vn/v1/user/logout", {
    username,
    password
  }, {
      headers: {
        "X-CSRF-Token": req.query.logoutToken,
        "Cookie": req.query.token
      },
    }).then(response => {
      res.send(Responser.getCustomSuccessMessage(String(response.data[0]), String(response.data[0]), "Logout success"));
    }).catch(error => {
      res.send(Responser.getCustomFailMessageByClient(error.response.data[0]));
    });
};

/**
 * GET api/module_registration
 * Timetable.
 */
export let apiGetModuleRegistration = (req: Request, res: Response) => {
  axios.get(`https://apiservice.uit.edu.vn/v1/data?task=TKB&hocky=${req.query.semester}&namhoc=${req.query.year}`, {
    headers: {
      "X-CSRF-Token": "f7QlqYd8wQdvtMJYK6nZyIsiafG_luk4luXx2eNjrKc",
      "Cookie": req.query.token
    },
  }).then(response => {
    if (response.data == undefined) {
      return res.send(Responser.getCustomFailMessageByServer("No data"));
    }

    const dataList = response.data as Array<moduleRegistration>;
    const data = [] as Array<moduleRegistrationBase>;
    dataList.forEach(item => {
      const teachers = [] as Array<teacher>;
      const baseData = {
        id: Number(item.id),
        class_alias: item.malop,
        class: item.tkb_mamh,
        subject: item.tenmh,
        room_name: item.phonghoc,
        period: Number(item.tiet),
        start_time: item.ngaybatdau,
        end_time: item.ngayketthuc,
        on_week: "unknown",
        year: Number(req.query.year),
        semester: Number(req.query.semester),
        day_of_the_week: Number(item.thu),
        credits: Number(item.sotc),
        faculty: item.khoaql,
        teaching_type: item.hinhthucgd,
        language: item.ngonngu
      } as moduleRegistrationBase;


      if (item.magv) {
        item.magv.forEach(teacher => {
          const teacherInfo = {
            codes: "unknown",
            name: teacher.hoten,
            phone: -1,
            faculty_name: item.khoaql,
            teacher_email: teacher.email,
          } as teacher;
          teachers.push(teacherInfo);
        });
      }
      baseData.teacher = teachers;
      data.push(baseData);
    });
    res.send(Responser.getCustomSuccessMessage(data, dataList, "Get module registration success"));
  }).catch(error => {
    res.send(Responser.getFailMessageByServer(error));
  });
};

/**
 * GET api/transcript
 * Transcript.
 */
export let apiGetTranscript = (req: Request, res: Response) => {
  // Get all
  if (req.query.semester == undefined && req.query.year == undefined) {
    axios.get(`https://apiservice.uit.edu.vn/v1/data?task=diem&hocky`, {
      headers: {
        "X-CSRF-Token": "f7QlqYd8wQdvtMJYK6nZyIsiafG_luk4luXx2eNjrKc",
        "Cookie": req.query.token
      },
    }).then(response => {
      if (response.data == undefined) {
        return res.send(Responser.getCustomFailMessageByServer("No data"));
      }

      const dataList = response.data as transcript;
      const data = [] as Array<transcriptBase>;
      dataList.diem.forEach(item => {
        const baseData = {
          semester: Number(item.hocky),
          title: getTitle(item.hocky, Number(item.namhoc)),
          class: item.mamh,
          class_name: item.tenmh,
          class_code: item.malop,
          credits: Number(item.sotc),
          process_mark: getMark(item.diem1),
          lab_mark: getMark(item.diem2),
          middle_mark: getMark(item.diem3),
          end_mark: getMark(item.diem4),
          final_mark: getMark(item.diem),
          note: item.ghichu,
          year: Number(item.namhoc),
        } as transcriptBase;
        data.push(baseData);
      });
      const semesterMark = {
        average_mark: dataList.diemtb.dtb,
        cgpa: dataList.diemtb.dtbtl,
        credits: dataList.diemtb.tctl
      } as any;
      const dataToSend = {
        marks: data,
        full_process_mark: semesterMark,
      } as any;
      return res.send(Responser.getCustomSuccessMessage(dataToSend, dataList, "Get transcript success"));
    }).catch(error => {
      return res.send(Responser.getFailMessageByServer(error));
    });
  }

  // Support params
  // axios.get(`https://apiservice.uit.edu.vn/v1/data?task=diem&hocky=${req.query.semester}&namhoc=${req.query.year}`, {
  //   headers: {
  //     "X-CSRF-Token": "f7QlqYd8wQdvtMJYK6nZyIsiafG_luk4luXx2eNjrKc",
  //     "Cookie": req.query.token
  //   },
  // }).then(response => {
  //   if (response.data == undefined) {
  //     return res.send(Responser.getCustomFailMessageByServer("No data"));
  //   }

  //   const dataList = response.data as transcript;
  //   const data = [] as Array<transcriptBase>;
  //   dataList.diem.forEach(item => {
  //     const baseData = {
  //       semester_number: Number(item.hocky),
  //       title: "unknown",
  //       class: item.mamh,
  //       class_name: item.tenmh,
  //       credits: Number(item.sotc),
  //       process_mark: getMark(item.diem1),
  //       lab_mark: getMark(item.diem2),
  //       middle_mark: getMark(item.diem3),
  //       end_mark: getMark(item.diem4),
  //       final_mark: getMark(item.diem),
  //       note: item.ghichu,
  //     } as transcriptBase;
  //     data.push(baseData);
  //   });
  //   const semesterMark = {
  //     average_mark: dataList.diemtb.dtb,
  //     cgpa: dataList.diemtb.dtbtl,
  //     credits: dataList.diemtb.tctl
  //   } as any;
  //   const dataToSend = [{
  //     marks: data,
  //     semester_mark: semesterMark,
  //   }] as any;
  //   res.send(Responser.getCustomSuccessMessage(dataToSend, dataList, "Get transcript success"));
  // }).catch(error => {
  //   res.send(Responser.getFailMessageByServer(error));
  // });
};


const getMark = (mark: string | number | null) => {
  if (mark == undefined) {
    // tslint:disable-next-line
    return null;
  } else {
    return Number(mark);
  }
};

const getTitle = (hocky: number, namhoc: number) => {
  return ["Học kỳ ", hocky, "|", "Năm học ", namhoc, "-", namhoc + 1].join("");
};




/*
 * GET api/fee
 * Fee.
 */
export let apiGetFee = (req: Request, res: Response) => {
  // Get list
  if (req.query.semester == undefined && req.query.year == undefined) {
    axios.get(`https://apiservice.uit.edu.vn/v1/data?task=HOCPHi`, {
      headers: {
        "X-CSRF-Token": "f7QlqYd8wQdvtMJYK6nZyIsiafG_luk4luXx2eNjrKc",
        "Cookie": req.query.token
      },
    }).then(response => {
      if (response.data == undefined) {
        return res.send(Responser.getCustomFailMessageByServer("No data"));
      }

      const dataSchool = response.data as Array<fee>;
      const data = [] as Array<feeBase>;
      dataSchool.forEach(item => {
        const baseData = {
          semester: Number(item.hocky),
          title: getTitle(item.hocky, Number(item.namhoc)),
          year: Number(item.namhoc),
          price: -1,
          price_to_pay: Number(item.phaidong),
          last_semester_debt: Number(item.notruoc),
          have_paid: Number(item.dadong),
          description: "unknown",
          module_registrations: item.dkhp,
          number_of_subjects: Number(item.somon),
          paid_time: item.thoigiandong,
          bank: item.nganhang,
          detail: item.dkhp,
          credits: -1,
        } as feeBase;
        data.push(baseData);
      });
      return res.send(Responser.getCustomSuccessMessage(data, dataSchool, "Get transcript success"));
    }).catch(error => {
      return res.send(Responser.getFailMessageByServer(error));
    });
  }
};


// const getCredits = (dkhp: String): Number => {
//   console.log(dkhp.split(",").map(item => {
//     return item.match(/\(\d+\)/) ? item.match(/\(\d+\)/)[0] : null;
//   }));
//   return 0;
// };



/*
 * GET api/info
 * Info.
 */
export let apiGetInfo = (req: Request, res: Response) => {
  // Get list
  axios.get(`https://apiservice.uit.edu.vn/v1/data?task=current`, {
    headers: {
      "X-CSRF-Token": "f7QlqYd8wQdvtMJYK6nZyIsiafG_luk4luXx2eNjrKc",
      "Cookie": req.query.token
    },
  }).then(response => {
    if (response.data == undefined) {
      return res.send(Responser.getCustomFailMessageByServer("No data"));
    }

    const item = response.data as info;

    const baseData: infoBase = {
      username: Number(item.sid),
      name: item.name,
      phone: -1,
      faculty_name: item.major,
      email: item.mail,
      avatar: ["https://student.uit.edu.vn/image.php?user=", Number(item.sid), "&id=", item.avatar].join(""),
      student_code: Number(item.sid),
      admission_year: Number(item.class.match(/\d+/).toString()),
      fcm_token: "unknown",
      birth_day: item.dob,
      class_type: "unknown",
      class_name: item.class,
      address: item.address,
      degree: "uknown",
      sex: "uknown",
      role: {
        name: item.role,
        access: "uknonwn"
      }
    };

    return res.send(Responser.getCustomSuccessMessage(baseData, item, "Get info success"));
  }).catch(error => {
    return res.send(Responser.getFailMessageByServer(error));
  });
};



/*
 * GET api/event
 * Event.
 */
export let apiGetEvent = (req: Request, res: Response) => {
  const data = [
    {
      "event_name": "[VNG-GSN] NHỮNG AI SẼ CÙNG GAME DEVELOPER TẠO NÊN MỘT SẢN PHẨM GAME HOÀN CHỈNH",
      "detail": "Với Game Developer, mỗi sản phẩm game luôn là đứa con tinh thần mà họ đã dày công chăm sóc và nâng niu suốt nhiều tháng ròng. Nhưng để có một sản phẩm game thành công, đó còn là công sức của nhiều người, nhiều vị trí công việc khác nhau trong một quy trình làm game hoàn chỉnh. Hãy cùng #VNGRecuitment tìm hiểu xem, nếu trở thành Game Developer ở VNG-GSN thì bạn sẽ được làm việc, được cùng tạo ra một sản phẩm game thành công với những ai, những vị trí nào nhé.",
      "image_url": "https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-9/49896959_2019550171469262_1957184715244961792_o.jpg?_nc_cat=105&_nc_ht=scontent.fsgn2-1.fna&oh=44388f297889b0678541957791c9a927&oe=5CCECA85",
      "register_url": "http://bit.ly/2GJh5Ns",
    },
    {
      "event_name": "Khảo sát lớp NT118.J21 Phát triển ứng dụng trên thiết bị di động",
      "detail": "Sau khi đăng ký học phần, thì lớp NT118.J21 Phát triển ứng dụng trên thiết bị di động hiện tại đang có sĩ số là 24 sinh viên. Theo quy định của nhà trường, nếu mở lớp này, sinh viên phải chịu mức học phí cao hơn bình thường (file đính kèm). Vì vậy thầy gửi các em phiếu khảo sát, bạn nào đồng ý học với mức học phí theo quy định của sĩ số lớp ít thì đánh dấu vào ô đồng ý theo tên mình. Hạn chót để thực hiện trong ngày mai 05/01/2019. Sau ngày mai dựa trên số lượng khảo sát, Khoa sẽ xem xét có mở lớp hay không. Cám ơn các em - thầy Sang",
      "image_url": "https://www.azuronaut.com/wp-content/uploads/2018/11/Survey-2-1024x10241.jpg",
      "register_url": "https://docs.google.com/spreadsheets/d/1U4lU9QolbxSH73vXMb576IwiTajepv8sz0AqHjY3AeU/edit?fbclid=IwAR0gVyyxyxp4gZ-10VaJtRXhIH-VB_lLpRJsbbyR1f9hoOAgp55OmSd7WYI#gid=1976510365",
    },
    {
      "event_name": "Lễ khánh thành và công bố Quyết định thành lập Khoa Khoa học và Kỹ thuật Thông tin",
      "detail": "Ngày 14 / 12 / 2018, Trường Đại học Công nghệ Thông tin(ĐH CNTT) long trọng tổ chức Lễ khánh thành và công bố Quyết định thành lập, bổ nhiệm Trưởng khoa Khoa học và Kỹ thuật Thông tin(KH & KTTT).Đây là một sự kiện có nhiều ý nghĩa, đánh dấu một cột mốc phát triển mới của Trường ĐH CNTT sau 12 năm thành lập. Tham dự buổi Lễ, về phía khách mời có sự tham dự của ThS.Nguyễn Thanh Nguyên, Phó Trưởng Ban Quan hệ Đối ngoại ĐHQG - HCM, và các đối tác doanh nghiệp của Khoa. Về phía Trường ĐH CNTT, buổi lễ có sự hiện diện Ban Giám hiệu, lãnh đạo Khoa, Phòng ban, và các Thầy Cô giáo của Khoa KH & KTTT.",
      "image_url": "https://www.uit.edu.vn/sites/vi/files/uploads/images/201812/tan_truong_khoa_doc_dien_van_phat_bieu.jpg",
      "register_url": "https://www.uit.edu.vn/le-khanh-thanh-va-cong-bo-quyet-dinh-thanh-lap-khoa-khoa-hoc-va-ky-thuat-thong-tin",
    },
    {
      "event_name": '[XTN2019] CHƯƠNG TRÌNH "TẾT SUM VẦY CÙNG KMS"',
      "detail": 'Chương trình Tặng vé xe Tết "Tết Đoàn Viên Cùng KMS" nằm trong khuôn khổ Chiến dịch Xuân Tình Nguyện UIT 2019 với sự tài trợ của Công ty KMS Technology phối hợp với Hội sinh viên trường ĐH Công nghệ Thông tin - ĐHQG-HCM tổ chức nhằm hỗ trợ cho các bạn sinh viên có hoàn cảnh khó khăn có điều kiện về quê ăn Tết, sum vầy cùng gia đình.',
      "image_url": "https://scontent.fsgn2-2.fna.fbcdn.net/v/t1.0-9/49393495_2010881689004361_3457885703382237184_o.jpg?_nc_cat=103&_nc_ht=scontent.fsgn2-2.fna&oh=2014c45bf3179ed01a75e15eba96a526&oe=5CCC83A2",
      "register_url": "https://forum.uit.edu.vn/node/532408?fbclid=IwAR05kyFC8I7oIBWyzsadX78rEVFariFsAVzJdCVGp7mjGDLE9vyTTG9IThc",
    },
    {
      "event_name": "Thông báo nhận hồ sơ đăng ký thi TOEIC đợt ngày 16.03.2019",
      "detail": "Đăng ký thi TOEIC tại Trường Đại học Công nghệ Thông tin do IIG VN - đại diện ETS (Hoa Kỳ) tổ chức. Thời gian thi: 16.03.2019. Thời gian đăng ký: từ ngày ra thông báo đến hết ngày 01.03.2019, từ 8h00 đến 16h00 (từ Thứ 2 đến Thứ 6). Lệ phí thi: 900.000 VND ...",
      "image_url": "https://duhocphi.vn/wp-content/uploads/2016/06/khoa-hoc-toeic-dam-bao.jpg",
      "register_url": "http://celuit.edu.vn/vi/thong-bao-nhan-ho-so-dang-ky-thi-toeic-dot-ngay-16032019",
    }
  ];
  return res.send(Responser.getCustomSuccessMessage(data, undefined, "Get events success"));
};




/*
 * GET api/contest
 * Contest.
 */
export let apiGetContest = (req: Request, res: Response) => {
  axios.get(`https://apiservice.uit.edu.vn/v1/data?task=EXAM`, {
    headers: {
      "X-CSRF-Token": "f7QlqYd8wQdvtMJYK6nZyIsiafG_luk4luXx2eNjrKc",
      "Cookie": req.query.token
    }
  }).then(response => {
    if (response.data == undefined) {
      return res.send(Responser.getCustomFailMessageByServer("No data"));
    }

    const dataSchool = response.data as Array<contest>;
    const data = [] as Array<contestBase>;

    for (const key of Object.keys(dataSchool)) {
      const item: contest = dataSchool[key];
      const baseData: contestBase = {
        day_of_the_week: Number(item.thu),
        on_day: item.ngaythi,
        period: Number(item.ca),
        room_name: item.phongthi,
        supervisory: "unknown",
        ordinal_number: -1,
        module_registration: {
          class_room: {
            class_alias: item.malop,
            subject: item.tenmh,
            year: -1,
            teacher: {
              name: "unknown"
            }
          }
        }
      };
      data.push(baseData);
    }
    return res.send(Responser.getCustomSuccessMessage(data, dataSchool, "Get contests success"));
  }).catch((e) => {
    console.log(e);
  });
};






/*
 * GET api/notifications
 * Notifications.
 */
export let apiGetNotification = (req: Request, res: Response) => {
  if (!req.query.date) {
    if (!moment(moment(req.query.date).format("YYYY-MM-DD"), "YYYY-MM-DD", true).isValid()) {
      return res.send(Responser.getCustomFailMessageByServer("Date is not valid"));
    }
    return res.send(Responser.getCustomFailMessageByServer("Date is required"));
  }

  axios.get(`https://apiservice.uit.edu.vn/v1/data?task=message`, {
    headers: {
      "X-CSRF-Token": "f7QlqYd8wQdvtMJYK6nZyIsiafG_luk4luXx2eNjrKc",
      "Cookie": req.query.token
    },
  }).then(response => {
    if (response.data == undefined) {
      return res.send(Responser.getCustomFailMessageByServer("No data"));
    }

    // Select where notification is larger than time required
    const notifcations = [] as Array<any>;
    const notifcationArray = response.data as any;
    for (const item of notifcationArray) {
      const date = moment(item.date_time);
      if (date.isSameOrAfter(req.query.date)) {
        notifcations.push(item);
      }
    }

    // Enriching data
    const notifcationstoSend = [] as Array<any>;
    for (const item of notifcations) {
      item["base_link"] = ["https://daa.uit.edu.vn/node/", item.nid].join("");
      if (item.type == 2) {
        item["title"] = ["Thông báo học bù lớp ", item.class_id].join("");
      } else if (item.type == 1) {
        item["title"] = ["Thông báo nghỉ lớp ", item.class_id].join("");
      }
      notifcationstoSend.push(item);
    }

    if (notifcationstoSend.length == 0) {
      return res.send(Responser.getCustomFailMessageByServer("No new notification"));
    }

    return res.send(Responser.getCustomSuccessMessage(notifcationstoSend, notifcationArray, "Get notifcations success"));
  }).catch(error => {
    return res.send(Responser.getFailMessageByServer(error));
  });
};



























/* Dont delete these */
/* use for code reference */


// /**
//  * GET /login
//  * Login page.
//  */
// export let getLogin = (req: Request, res: Response) => {
//   if (req.user) {
//     return res.redirect("/");
//   }
//   res.render("account/login", {
//     title: "Login"
//   });
// };

// /**
//  * POST /login
//  * Sign in using email and password.
//  */
// export let postLogin = (req: Request, res: Response, next: NextFunction) => {
//   req.assert("email", "Email is not valid").isEmail();
//   req.assert("password", "Password cannot be blank").notEmpty();
//   req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });

//   const errors = req.validationErrors();

//   if (errors) {
//     req.flash("errors", errors);
//     return res.redirect("/login");
//   }

//   passport.authenticate("local", (err: Error, user: UserModel, info: IVerifyOptions) => {
//     if (err) { return next(err); }
//     if (!user) {
//       req.flash("errors", info.message);
//       return res.redirect("/login");
//     }
//     req.logIn(user, (err) => {
//       if (err) { return next(err); }
//       req.flash("success", { msg: "Success! You are logged in." });
//       res.redirect(req.session.returnTo || "/");
//     });
//   })(req, res, next);
// };

// /**
//  * GET /logout
//  * Log out.
//  */
// export let logout = (req: Request, res: Response) => {
//   req.logout();
//   res.redirect("/");
// };

// /**
//  * GET /signup
//  * Signup page.
//  */
// export let getSignup = (req: Request, res: Response) => {
//   if (req.user) {
//     return res.redirect("/");
//   }
//   res.render("account/signup", {
//     title: "Create Account"
//   });
// };

// /**
//  * POST /signup
//  * Create a new local account.
//  */
// export let postSignup = (req: Request, res: Response, next: NextFunction) => {
//   req.assert("email", "Email is not valid").isEmail();
//   req.assert("password", "Password must be at least 4 characters long").len({ min: 4 });
//   req.assert("confirmPassword", "Passwords do not match").equals(req.body.password);
//   req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });

//   const errors = req.validationErrors();

//   if (errors) {
//     req.flash("errors", errors);
//     return res.redirect("/signup");
//   }

//   const user = new User({
//     email: req.body.email,
//     password: req.body.password
//   });

//   User.findOne({ email: req.body.email }, (err, existingUser) => {
//     if (err) { return next(err); }
//     if (existingUser) {
//       req.flash("errors", { msg: "Account with that email address already exists." });
//       return res.redirect("/signup");
//     }
//     user.save((err) => {
//       if (err) { return next(err); }
//       req.logIn(user, (err) => {
//         if (err) {
//           return next(err);
//         }
//         res.redirect("/");
//       });
//     });
//   });
// };


// /**
//  * POST /account/profile
//  * Update profile information.
//  */
// export let postUpdateProfile = (req: Request, res: Response, next: NextFunction) => {
//   req.assert("email", "Please enter a valid email address.").isEmail();
//   req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });

//   const errors = req.validationErrors();

//   if (errors) {
//     req.flash("errors", errors);
//     return res.redirect("/account");
//   }

//   User.findById(req.user.id, (err, user: UserModel) => {
//     if (err) { return next(err); }
//     user.email = req.body.email || "";
//     user.profile.name = req.body.name || "";
//     user.profile.gender = req.body.gender || "";
//     user.profile.location = req.body.location || "";
//     user.profile.website = req.body.website || "";
//     user.save((err: WriteError) => {
//       if (err) {
//         if (err.code === 11000) {
//           req.flash("errors", { msg: "The email address you have entered is already associated with an account." });
//           return res.redirect("/account");
//         }
//         return next(err);
//       }
//       req.flash("success", { msg: "Profile information has been updated." });
//       res.redirect("/account");
//     });
//   });
// };

// /**
//  * POST /account/password
//  * Update current password.
//  */
// export let postUpdatePassword = (req: Request, res: Response, next: NextFunction) => {
//   req.assert("password", "Password must be at least 4 characters long").len({ min: 4 });
//   req.assert("confirmPassword", "Passwords do not match").equals(req.body.password);

//   const errors = req.validationErrors();

//   if (errors) {
//     req.flash("errors", errors);
//     return res.redirect("/account");
//   }

//   User.findById(req.user.id, (err, user: UserModel) => {
//     if (err) { return next(err); }
//     user.password = req.body.password;
//     user.save((err: WriteError) => {
//       if (err) { return next(err); }
//       req.flash("success", { msg: "Password has been changed." });
//       res.redirect("/account");
//     });
//   });
// };

// /**
//  * POST /account/delete
//  * Delete user account.
//  */
// export let postDeleteAccount = (req: Request, res: Response, next: NextFunction) => {
//   User.remove({ _id: req.user.id }, (err) => {
//     if (err) { return next(err); }
//     req.logout();
//     req.flash("info", { msg: "Your account has been deleted." });
//     res.redirect("/");
//   });
// };

// /**
//  * GET /account/unlink/:provider
//  * Unlink OAuth provider.
//  */
// export let getOauthUnlink = (req: Request, res: Response, next: NextFunction) => {
//   const provider = req.params.provider;
//   User.findById(req.user.id, (err, user: any) => {
//     if (err) { return next(err); }
//     user[provider] = undefined;
//     user.tokens = user.tokens.filter((token: AuthToken) => token.kind !== provider);
//     user.save((err: WriteError) => {
//       if (err) { return next(err); }
//       req.flash("info", { msg: `${provider} account has been unlinked.` });
//       res.redirect("/account");
//     });
//   });
// };

// /**
//  * GET /reset/:token
//  * Reset Password page.
//  */
// export let getReset = (req: Request, res: Response, next: NextFunction) => {
//   if (req.isAuthenticated()) {
//     return res.redirect("/");
//   }
//   User
//     .findOne({ passwordResetToken: req.params.token })
//     .where("passwordResetExpires").gt(Date.now())
//     .exec((err, user) => {
//       if (err) { return next(err); }
//       if (!user) {
//         req.flash("errors", { msg: "Password reset token is invalid or has expired." });
//         return res.redirect("/forgot");
//       }
//       res.render("account/reset", {
//         title: "Password Reset"
//       });
//     });
// };

// /**
//  * POST /reset/:token
//  * Process the reset password request.
//  */
// export let postReset = (req: Request, res: Response, next: NextFunction) => {
//   req.assert("password", "Password must be at least 4 characters long.").len({ min: 4 });
//   req.assert("confirm", "Passwords must match.").equals(req.body.password);

//   const errors = req.validationErrors();

//   if (errors) {
//     req.flash("errors", errors);
//     return res.redirect("back");
//   }

//   async.waterfall([
//     function resetPassword(done: Function) {
//       User
//         .findOne({ passwordResetToken: req.params.token })
//         .where("passwordResetExpires").gt(Date.now())
//         .exec((err, user: any) => {
//           if (err) { return next(err); }
//           if (!user) {
//             req.flash("errors", { msg: "Password reset token is invalid or has expired." });
//             return res.redirect("back");
//           }
//           user.password = req.body.password;
//           user.passwordResetToken = undefined;
//           user.passwordResetExpires = undefined;
//           user.save((err: WriteError) => {
//             if (err) { return next(err); }
//             req.logIn(user, (err) => {
//               done(err, user);
//             });
//           });
//         });
//     },
//     function sendResetPasswordEmail(user: UserModel, done: Function) {
//       const transporter = nodemailer.createTransport({
//         service: "SendGrid",
//         auth: {
//           user: process.env.SENDGRID_USER,
//           pass: process.env.SENDGRID_PASSWORD
//         }
//       });
//       const mailOptions = {
//         to: user.email,
//         from: "express-ts@starter.com",
//         subject: "Your password has been changed",
//         text: `Hello,\n\nThis is a confirmation that the password for your account ${user.email} has just been changed.\n`
//       };
//       transporter.sendMail(mailOptions, (err) => {
//         req.flash("success", { msg: "Success! Your password has been changed." });
//         done(err);
//       });
//     }
//   ], (err) => {
//     if (err) { return next(err); }
//     res.redirect("/");
//   });
// };

// /**
//  * GET /forgot
//  * Forgot Password page.
//  */
// export let getForgot = (req: Request, res: Response) => {
//   if (req.isAuthenticated()) {
//     return res.redirect("/");
//   }
//   res.render("account/forgot", {
//     title: "Forgot Password"
//   });
// };

// /**
//  * POST /forgot
//  * Create a random token, then the send user an email with a reset link.
//  */
// export let postForgot = (req: Request, res: Response, next: NextFunction) => {
//   req.assert("email", "Please enter a valid email address.").isEmail();
//   req.sanitize("email").normalizeEmail({ gmail_remove_dots: false });

//   const errors = req.validationErrors();

//   if (errors) {
//     req.flash("errors", errors);
//     return res.redirect("/forgot");
//   }

//   async.waterfall([
//     function createRandomToken(done: Function) {
//       crypto.randomBytes(16, (err, buf) => {
//         const token = buf.toString("hex");
//         done(err, token);
//       });
//     },
//     function setRandomToken(token: AuthToken, done: Function) {
//       User.findOne({ email: req.body.email }, (err, user: any) => {
//         if (err) { return done(err); }
//         if (!user) {
//           req.flash("errors", { msg: "Account with that email address does not exist." });
//           return res.redirect("/forgot");
//         }
//         user.passwordResetToken = token;
//         user.passwordResetExpires = Date.now() + 3600000; // 1 hour
//         user.save((err: WriteError) => {
//           done(err, token, user);
//         });
//       });
//     },
//     function sendForgotPasswordEmail(token: AuthToken, user: UserModel, done: Function) {
//       const transporter = nodemailer.createTransport({
//         service: "SendGrid",
//         auth: {
//           user: process.env.SENDGRID_USER,
//           pass: process.env.SENDGRID_PASSWORD
//         }
//       });
//       const mailOptions = {
//         to: user.email,
//         from: "hackathon@starter.com",
//         subject: "Reset your password on Hackathon Starter",
//         text: `You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n
//           Please click on the following link, or paste this into your browser to complete the process:\n\n
//           http://${req.headers.host}/reset/${token}\n\n
//           If you did not request this, please ignore this email and your password will remain unchanged.\n`
//       };
//       transporter.sendMail(mailOptions, (err) => {
//         req.flash("info", { msg: `An e-mail has been sent to ${user.email} with further instructions.` });
//         done(err);
//       });
//     }
//   ], (err) => {
//     if (err) { return next(err); }
//     res.redirect("/forgot");
//   });
// };
