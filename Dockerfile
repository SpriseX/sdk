FROM node:8
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8082
# RUN npm rebuild node-sass --force
CMD [ "npm", "run", "custom-watch" ]